import java.util.Date;
import java.util.LinkedList;
import java.util.Locale;

public class Human {
    public String firstName;
    public String lastName;
    public String patronymic;
    public int age;
    public String male;
    public String date;

    public Human(String firstName, String lastName, String patronymic, int age, String male, String date) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.patronymic = patronymic;
        this.age = age;
        this.male = male;
        this.date = date;
    }

}
